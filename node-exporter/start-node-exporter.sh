#!/bin/bash

docker rm -f nexporter || true

docker pull prom/node-exporter

docker run -d --name nexporter \
  --restart always \
  -v "/proc:/host/proc:ro" \
  -v "/sys:/host/sys:ro" \
  -v "/:/rootfs:ro" \
  --net="host" \
  prom/node-exporter \
  --path.procfs=/host/proc \
  --path.sysfs=/host/sys \
  --path.rootfs=/rootfs \
  '--collector.filesystem.ignored-mount-points=^/(sys|proc|dev|host|etc)($$|/)'

