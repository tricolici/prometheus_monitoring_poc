#!/bin/bash

sdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if [ ! -f "$sdir/prometheus.yml" ]; then
  echo 'Please create prometheus.yml.'
  exit 1
fi

docker rm -f prometheus || true

if [ "$1" == "clean" ]; then
  echo "remove all prometheus data"
  rm -rf "$sdir/data"
fi

mkdir -p "$sdir/data"

docker pull prom/prometheus

docker run -d --name prometheus \
  --restart always \
  --user $(id -u):$(id -g) \
  --net="host" \
  -v "$sdir/data:/data/prometheus" \
  -v "$sdir/prometheus.yml:/etc/prometheus/prometheus.yml:ro" \
  prom/prometheus \
  --config.file="/etc/prometheus/prometheus.yml" \
  --storage.tsdb.path="/data/prometheus"

