#!/bin/bash

# https://stackoverflow.com/questions/59895/get-the-source-directory-of-a-bash-script-from-within-the-script-itself
sdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

docker rm -f grafana

if [ "$1" == "clean" ]; then
  echo "remove all grafana data"
  rm -rf "$sdir/data"
fi

mkdir -p "$sdir/data"

docker run -d --name grafana \
  --user $(id -u):$(id -g) \
  -v "$sdir/data:/var/lib/grafana" \
  --net="host" \
  -e "GF_INSTALL_PLUGINS=grafana-piechart-panel" \
  grafana/grafana

docker logs -f grafana
